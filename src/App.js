import logo from './logo.svg';
import './App.css';
import CPFInput from './CPFInput.js';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Validar CPF:
        </p>
        <CPFInput />
      </header>
    </div>
  );
}

export default App;
